using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using Cinemachine;

public class SwitchVcam : MonoBehaviour
{
    [SerializeField] private PlayerInput playerInput;
    [SerializeField] private int priorityBoostAmount = 10;
    //[SerializeField] private Canvas thirPersonCanvas;
    [SerializeField] private Canvas aimCanvas;
    private InputAction aimACtion;
    private CinemachineVirtualCamera virtualCamera;

    private void Awake()
    {
        virtualCamera = GetComponent<CinemachineVirtualCamera>();
        aimACtion = playerInput.actions["Aim"];
    }

    private void OnEnable()
    {
        aimACtion.performed +=  _=> StartAim();
        aimACtion.canceled +=  _=> CancelAim();
    }

    private void OnDisable()
    {
        aimACtion.performed -=  _=> StartAim();
        aimACtion.canceled -=  _=> CancelAim();
    }

    void StartAim()
    {
        virtualCamera.Priority += priorityBoostAmount;
        aimCanvas.enabled = true;
        //thirPersonCanvas.enabled = false;

    }

    void CancelAim()
    {
        virtualCamera.Priority -= priorityBoostAmount;
        aimCanvas.enabled = false;
        //thirPersonCanvas.enabled = true;
    }
}